from sentinelc_appfeed.validator import validate_app_name, validate_version


def test_validate_app_name():
    assert not validate_app_name("No")
    assert not validate_app_name("all--all")
    assert not validate_app_name("-no")
    assert not validate_app_name("no-")
    assert not validate_app_name("-")
    assert not validate_app_name("all/no")
    assert not validate_app_name("all_no")

    assert validate_app_name("1click")
    assert validate_app_name("click1")
    assert validate_app_name("1cli1ck")


def test_validate_version():
    assert validate_version("v20240101")
    assert validate_version("v20240101-r1")
    assert validate_version("1.0")
    assert validate_version("v1.0")
    assert validate_version("v1")
    assert validate_version("v1.0.1")
    assert validate_version("v1.0.1-r1")
    assert validate_version("v1.0.1-beta1")

    assert not validate_version("V1")
    assert not validate_version("1/0")
    assert not validate_version("1_0")
    assert not validate_version("B1")
    assert not validate_version("ALLO")
    assert not validate_version("@1")
    assert not validate_version("%1")
