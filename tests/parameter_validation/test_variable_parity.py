import os

import pytest
from sentinelc_appfeed.builder import build_feed

from sentinelc_appfeed.validator import ValidationError


BASEPATH = f"{os.path.dirname(os.path.abspath(__file__))}/data/variable_mapping"


def test_variable_set():
    feed = build_feed(f"{BASEPATH}/variable-set")
    assert feed["apps"][0]["vars"]["timezone"]["default"] == "America/NewYork"


def test_variable_missing_template():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/variable-missing")
    assert (
        str(exc_info.value)
        == "Vars used in template do not match the defined ones: missing: timezone extra: ."
    )


def test_variable_missing():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/template-missing")
    assert str(exc_info.value) == "Missing mandatory field(s)"
