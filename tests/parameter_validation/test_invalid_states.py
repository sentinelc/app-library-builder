import os

import pytest
from sentinelc_appfeed.builder import build_feed

from sentinelc_appfeed.validator import ValidationError


BASEPATH = f"{os.path.dirname(os.path.abspath(__file__))}/data"


def test_missing_kube():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/missing_kube")
    assert "hello-world.kube.yml is missing" in str(exc_info.value)


def test_validate_auto_immutable():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/auto_mutable")
    assert str(exc_info.value) == "timezone in App hello-world can't be automatic and mutable"


def test_validate_reveal_not_secret():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/reveal_not_secret")
    assert (
        str(exc_info.value)
        == "timezone in App hello-world doesn't need to be revealed_once if not secret"
    )


def test_valid_types():
    feed = build_feed(f"{BASEPATH}/types/valid")
    assert feed["apps"][0]["vars"] == {
        "text": {
            "type": "text",
            "label": {"en": "text"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "checkbox": {
            "type": "checkbox",
            "label": {"en": "checkbox"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "number": {
            "type": "number",
            "label": {"en": "number"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "password": {
            "type": "password",
            "label": {"en": "password"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "email": {
            "type": "email",
            "label": {"en": "email"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "url": {
            "type": "url",
            "label": {"en": "url"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
        "textarea": {
            "type": "textarea",
            "label": {"en": "textarea"},
            "description": {},
            "required": False,
            "regexp": None,
            "default": "",
            "auto": False,
            "secret": False,
            "immutable": False,
            "reveal_once": False,
        },
    }


def test_invalid_type():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/types/invalid")
    assert str(exc_info.value) == "Var invalid in hello-world: Invalid type: invalid"


def test_valid_regexp():
    feed = build_feed(f"{BASEPATH}/regexp/valid")
    app = feed["apps"][0]
    assert app["vars"]["regexp"]["regexp"] == "\\d*[a-zA-Z]*"


def test_invalid_regexp():
    with pytest.raises(ValidationError) as exc_info:
        build_feed(f"{BASEPATH}/regexp/invalid")
    assert str(exc_info.value) == "regexp in App hello-world contains an invalid regular expression"
