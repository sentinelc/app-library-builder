import os

from sentinelc_appfeed.builder import build_feed

BASEPATH = f"{os.path.dirname(os.path.abspath(__file__))}/data"


def test_optional_homepage_documentation():
    feed = build_feed(f"{BASEPATH}/optional_homepage_documentation")
    app = feed["apps"][0]
    assert app["homepage"] is None
