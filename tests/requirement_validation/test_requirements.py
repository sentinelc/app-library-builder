import os
from sentinelc_appfeed.builder import build_feed

BASEPATH = f"{os.path.dirname(os.path.abspath(__file__))}/data"


def test_values_should_be_null():
    feed = build_feed(f"{BASEPATH}/empty")
    app = feed["apps"][0]
    assert app["requirements"]["storage"] is None
    assert app["requirements"]["memory"] is None


def test_requirements_contains_values():
    feed = build_feed(f"{BASEPATH}/contain-values")
    app = feed["apps"][0]
    assert app["requirements"]["storage"] == 1000000000
    assert app["requirements"]["memory"] == 500000000
