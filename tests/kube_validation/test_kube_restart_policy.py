import pytest
import yaml

from sentinelc_appfeed.validator import ValidationError, validate_kube_yaml


INVALID = """
apiVersion: v1
kind: Pod
metadata:
  name: hello-world
spec:
  restartPolicy: OnFailure
  containers:
  - image: docker.io/library/hello-world:latest
    name: hello-world
status: {}
"""


VALID_ALWAYS = """
apiVersion: v1
kind: Pod
metadata:
  name: hello-world
spec:
  restartPolicy: Always
  containers:
  - image: docker.io/library/hello-world:latest
    name: hello-world
status: {}
"""


VALID_EMPTY = """
apiVersion: v1
kind: Pod
metadata:
  name: hello-world
spec:
  containers:
  - image: docker.io/library/hello-world:latest
    name: hello-world
status: {}
"""


def test_invalid():
    with pytest.raises(ValidationError) as ex_info:
        validate_kube_yaml(INVALID, "fakepath.yml")

    assert str(ex_info.value) == "fakepath.yml: restartPolicy can only be set to Always."


def test_valid_always():
    assert validate_kube_yaml(VALID_ALWAYS, "fakepath.yml") == yaml.safe_load(VALID_ALWAYS)


def test_valid_empty():
    assert validate_kube_yaml(VALID_EMPTY, "fakepath.yml") == yaml.safe_load(VALID_EMPTY)
