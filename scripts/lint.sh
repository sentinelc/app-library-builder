#!/bin/bash
black --fast -t py37 -t py38 -l 100 scripts/ sentinelc_appfeed/ tests/
flake8 scripts/ sentinelc_appfeed/ tests/
