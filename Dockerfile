FROM python:3.12.2

USER root

ADD requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt

ADD scripts/ /scripts/
WORKDIR /scripts/

RUN mkdir /src/
ADD sentinelc_appfeed/ /src/sentinelc_appfeed/
ADD setup.py /src/
ADD MANIFEST.in /src/
ADD README.md /src/
RUN pip3 install /src/

CMD applib-builder

